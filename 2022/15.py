from aocd import get_data, submit

YEAR = 2022
DAY = 15

if __name__ == "__main__":
    data = get_data(day=DAY, year=YEAR).split("\n")
    submit(__answer_A__, part='a', day=DAY, year=YEAR)
    submit(__answer_B__, part='b', day=DAY, year=YEAR)
