from aocd import get_data, submit
from collections import defaultdict
from heapq import heappush, heappop

YEAR = 2022
DAY = 12


def inf(cell):
    if cell == 'E':
        return 'y'
    else:
        return chr(ord(cell)-1)


def parse(data):
    G = defaultdict(list)
    for i in range(len(data)):
        line = data[i]
        for j in range(len(line)):
            cell = line[j]
            if cell == 'E':
                end = (i, j)
                continue
            elif cell == 'S':
                cell = 'a'
                start = (i, j)
            if i > 0 and inf(data[i-1][j]) <= cell:
                G[(i-1, j)].append((i, j))
            if i < len(data) - 1 and inf(data[i+1][j]) <= cell:
                G[(i+1, j)].append((i, j))
            if j > 0 and inf(line[j-1]) <= cell:
                G[(i, j-1)].append((i, j))
            if j < len(line) - 1 and inf(line[j+1]) <= cell:
                G[(i, j+1)].append((i, j))
    return G, start, end


def dijkstra(g, start):
    dist = {start: 0}
    next = [(0, start)]
    while next:
        dx, x = heappop(next)
        for neighbor in g[x]:
            if neighbor not in dist.keys() or dist[neighbor] > dx + 1:
                dist[neighbor] = dx + 1
                heappush(next, (dx + 1, neighbor))
    return dist


data = get_data(day=DAY, year=YEAR).split("\n")

G, start, end = parse(data)
distance = dijkstra(G, end)
submit(distance[start], part='a', day=DAY, year=YEAR)

min = distance[start]
for i in range(len(data)):
    line = data[i]
    for j in range(len(line)):
        cell = line[j]
        if cell == 'a':
            if (i, j) in distance.keys() and distance[(i, j)] < min:
                min = distance[(i, j)]
submit(min, part='b', day=DAY, year=YEAR)
