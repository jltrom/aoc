
# Advent of code 2022

## [Day 1: Calorie Counting](https://adventofcode.com/2022/day/1) ✨

I give a try to [advent-of-code-data](https://pypi.org/project/advent-of-code-data/ ) to fetch the data. It works as expected. thanks! You just need to defined the variable `AOC_SESSION` or put it in a file `~/.config/aocd/token`

Usage: 
```
python 01.py
```

[code](01.py)

## [Day 2: Rock Paper Scissors](https://adventofcode.com/2022/day/2) ✨

Implementing submission of the answer from code. It's blazzzzing fast.

Usage: 
```
python 02.py

That's the right answer!  You are one gold star closer to collecting enough star fruit. [Continue to Part Two]
That's the right answer!  You are one gold star closer to collecting enough star fruit. You have completed Day 2! You can [Share on  Twitter Mastodon] this victory or [Return to Your Advent Calendar].
```

[code](02.py)

## [Day 3: Rucksack Reorganization](https://adventofcode.com/2022/day/3) ✨

Using [doctest](https://docs.python.org/3/library/doctest.html) like a pro.

Usage:
```
❯ python -m doctest -v 03.py
Trying:
    [priority(i) for i in 'azAZ']
Expecting:
    [1, 26, 27, 52]
ok
Trying:
    priority_group('vJrwpWtwJgWrhcsFMMfFFhFp', 'jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL', 'PmmdzqPrVvPwwTWBwg')
Expecting:
    18
ok
Trying:
    priority_group('aaa' , 'abc', 'ddd')
Expecting:
    Traceback (most recent call last):
        ...
    Exception: Sticker not found
ok
Trying:
    priority_rucksack('vJrwpWtwJgWrhcsFMMfFFhFp')
Expecting:
    16
ok
Trying:
    priority_rucksack('aaaBBB')
Expecting:
    Traceback (most recent call last):
        ...
    Exception: Common item not found
ok
1 items had no tests:
    day3
3 items passed all tests:
   1 tests in day3.priority
   2 tests in day3.priority_group
   2 tests in day3.priority_rucksack
5 tests in 4 items.
5 passed and 0 failed.
Test passed.
```

[code](03.py)

## [Day 4: Camp Cleanup](https://adventofcode.com/2022/day/4) ✨

To avoid too many test conditions later, I choose to reorder the pair while parsing to be sure the first assignment starts with a lower section than the second.

[code](04.py)

## [Day 5: Supply Stacks](https://adventofcode.com/2022/day/5) ✨

I used a dict of list to store the stacks composition and then I process the instructions line by line using `.pop()` and `.append()`.
I considered using regex to parse the line, but why use regex when a simple split can do the trick ?
CrateMover 9001 was rather unexpected forcing me to do a deepcopy of the starting stacks composition for exo B.

[code](05.py)

## [Day 6: Tuning Trouble](https://adventofcode.com/2022/day/6) ✨

Exo B was a very simple variation of exo A. Long live genericity.

[code](06.py)

## [Day 7: No Space Left On Device](https://adventofcode.com/2022/day/7) ✨

> To understand recursion, one must first understand recursion.
> 
> -- <cite>Stephen Hawking

[code](07.py)

## [Day 8: Treetop Tree House](https://adventofcode.com/2022/day/8) ✨

I implemented a double index scan for exo A to take care of the complexity. 
But I didn't see a similar solution for exo B so I rushed with some dirty code ^^.

[code](08.py)


## [Day 9: Rope Bridge](https://adventofcode.com/2022/day/9) ✨

I enjoyed today's exercise. The main difficulty was to understand the movement of the 
rope and I am happy to have managed to implement it in a few lines.

[code](09.py)

## [Day 10: Cathode-Ray Tube](https://adventofcode.com/2022/day/10) ✨

```❯ python -m 2022.10
###  ####  ##  ###  #  # ###  #### ### #
#  #    # #  # #  # # #  #  # #    #  ##
#  #   #  #    #  # ##   #  # ###  ###  
###   #   # ## ###  # #  ###  #    #  # 
#    #    #  # #    # #  #    #    #  ##
#    ####  ### #    #  # #    #### ###  
Part a already solved with same answer: 13680
```

[code](10.py)


## [Day 11: Monkey in the Middle](https://adventofcode.com/2022/day/11) ✨

Today, an exercise that gives pride of place to arithmetic optimization. There were important clues in the statement...

[code](11.py)

## [Day 12: Hill Climbing Algorithm](https://adventofcode.com/2022/day/12) ✨

Graph algorithm for the beast :p

[code](12.py)