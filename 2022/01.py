from heapq import heappush, nlargest
from aocd.models import Puzzle
from aocd import submit

YEAR = 2022
DAY = 1

puzzle = Puzzle(year=YEAR, day=DAY)
acc = 0
elves = []

for line in puzzle.input_data.split("\n"):
    if line == "":
        heappush(elves, acc)
        acc = 0
    else:
        acc += int(line)
heappush(elves, acc)

top3 = nlargest(3, elves)

submit(top3[0], part='a', day=DAY, year=YEAR)
submit(sum(top3), part='b', day=DAY, year=YEAR)
