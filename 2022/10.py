from aocd import get_data, submit

YEAR = 2022
DAY = 10

def beamer(cycle, step, x):
    r = 0
    for _ in range(step):
        cycle += 1
        if cycle in [20, 60, 100, 140, 180, 220]:
            r = cycle * x
        p = cycle % 40
        if x <= p <= x+2:
            print('#', end='')
        else:
            print(' ', end='')
        if p == 0:
            print()
    return r, cycle


data = get_data(day=DAY, year=YEAR).split("\n")

x = 1
cycle = 0
signal = 0

for line in data:
    instruction = line.split(" ")
    if instruction[0] == "noop":
        r, cycle = beamer(cycle, 1, x)
    else:
        r, cycle = beamer(cycle, 2, x)
        x += int(instruction[1])
    signal += r

submit(signal, part='a', day=DAY, year=YEAR)
