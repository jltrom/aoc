from aocd import get_data, submit
import re
from collections import deque
from operator import pow, add, mul, floordiv, mod
from functools import reduce

YEAR = 2022
DAY = 11


class Monkey:

    def __init__(self, id, items, operator, param, divisor, iftrue, iffalse):
        self.id = id
        self.items = deque(items)
        self.operator = operator
        self.param = param
        self.divisor = divisor
        self.iftrue = iftrue
        self.iffalse = iffalse
        self.inspections = 0

    def throws(self, reducer, relief):
        while self.items:
            old = self.items.popleft()
            self.inspections += 1
            level = reducer(self.operator(old, self.param), relief)
            if level % self.divisor == 0:
                yield level, self.iftrue
            else:
                yield level, self.iffalse


def parse(data):
    monkeys = []
    nb_monkeys = (len(data)+1)//7
    for i in range(nb_monkeys):
        items = [int(nbr) for nbr in re.findall('[0-9]+',  data[i*7+1])]
        operation = data[i*7+2].split("=")[1]
        if operation == " old * old":
            op, param = pow, 2
        else:
            param = int(re.findall('[0-9]+', operation)[0])
            if operation.find("+") > 0:
                op = add
            else:
                op = mul
        divisor = int(re.findall('[0-9]+', data[i*7+3])[0])
        iftrue = int(re.findall('[0-9]+', data[i*7+4])[0])
        iffalse = int(re.findall('[0-9]+', data[i*7+5])[0])
        monkeys.append(Monkey(i, items, op, param, divisor, iftrue, iffalse))
    return monkeys

def run(monkeys, rounds, reducer, relief):
    for round in range(1, rounds + 1):
        for monkey in monkeys:
            for level, dest in monkey.throws(reducer, relief):
                monkeys[dest].items.append(level)

    activities = [m.inspections for m in monkeys]
    activities.sort(reverse=True)
    business = activities[0] * activities[1]

    return business

data = get_data(day=DAY, year=YEAR).split("\n")

monkeys = parse(data)
business = run(monkeys, 20, floordiv, 3)
submit(business, part='a', day=DAY, year=YEAR)

monkeys = parse(data)
modulo = reduce(mul, [m.divisor for m in monkeys])
business = run(monkeys, 10000, mod, modulo)
submit(business, part='b', day=DAY, year=YEAR)
