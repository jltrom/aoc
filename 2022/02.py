from aocd.models import Puzzle
from aocd import submit

YEAR = 2022
DAY = 2

ROCK = 1
PAPER = 2
SCISSORS = 3
LOST = 0
DRAW = 3
WIN = 6

SCORES_MAP = {
    "A X": ROCK + DRAW,
    "A Y": PAPER + WIN,
    "A Z": SCISSORS + LOST,
    "B X": ROCK + LOST,
    "B Y": PAPER + DRAW,
    "B Z": SCISSORS + WIN,
    "C X": ROCK + WIN,
    "C Y": PAPER + LOST,
    "C Z": SCISSORS + DRAW
}

SCORES_MAP2 = {
    "A X": LOST + SCISSORS,
    "A Y": DRAW + ROCK,
    "A Z": WIN + PAPER,
    "B X": LOST + ROCK,
    "B Y": DRAW + PAPER,
    "B Z": WIN + SCISSORS,
    "C X": LOST + PAPER,
    "C Y": DRAW + SCISSORS,
    "C Z": WIN + ROCK
}

puzzle = Puzzle(year=YEAR, day=DAY)
score = sum(SCORES_MAP[blow] for blow in puzzle.input_data.split("\n"))
score2 = sum(SCORES_MAP2[blow] for blow in puzzle.input_data.split("\n"))

submit(score, part='a', day=DAY, year=YEAR)
submit(score2, part='b', day=DAY, year=YEAR)
