from aocd.models import Puzzle
from aocd import submit

YEAR = 2022
DAY = 6


def find_marker(buffer, size):
    """Return the position of the marker (first group of [size] distinct letters).

    Examples:
    >>> find_marker('mjqjpqmgbljsphdztnvjfqwrcgsmlb', 4)
    7
    >>> find_marker('bbcde', 4)
    5
    >>> find_marker('bbcde', 14)
    Traceback (most recent call last):
        ...
    Exception: Marker not found
    >>> find_marker('abzzabacbabac', 4)
    Traceback (most recent call last):
        ...
    Exception: Marker not found
    """
    cursor = size
    while cursor <= len(buffer):
        if len(set(buffer[cursor-size:cursor])) == size:
            return cursor
        cursor += 1
    raise Exception("Marker not found")


if __name__ == "__main__":
    puzzle = Puzzle(year=YEAR, day=DAY)
    submit(find_marker(puzzle.input_data, 4), part='a', day=DAY, year=YEAR)
    submit(find_marker(puzzle.input_data, 14), part='b', day=DAY, year=YEAR)
