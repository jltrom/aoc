from aocd.models import Puzzle
from aocd import submit
import copy

YEAR = 2022
DAY = 5

puzzle = Puzzle(year=YEAR, day=DAY)
data = puzzle.input_data.split("\n")

start_procedure = data.index("")
stack_map = data[start_procedure-1]

stacks = {}
stacks_pos = {}
for i in range(len(stack_map)):
    if stack_map[i] != ' ':
        stacks[stack_map[i]] = []
        stacks_pos[stack_map[i]] = i

# parse starting position
for i in range(start_procedure-2, -1, -1):
    line = data[i]
    for k, v in stacks.items():
        if line[stacks_pos[k]] != " ":
            v.append(line[stacks_pos[k]])

# keep a copy of the starting stacks for exo B
stacks_B = copy.deepcopy(stacks)

# parse and proceed procedure # CrateMover 9000 / One crate at a time
for line in data[start_procedure+1:]:
    _, count, _, orig, _, dest = line.split(" ")
    for _ in range(int(count)):
        stacks[dest].append(stacks[orig].pop())

# submit top stacks
top_stacks = ""
for stack in stacks.values():
    top_stacks += stack[-1]
submit(top_stacks, part='a', day=DAY, year=YEAR)

# parse and proceed procedure # CrateMover 9001 / Many crates at a time
for line in data[start_procedure+1:]:
    _, count, _, orig, _, dest = line.split(" ")
    count = int(count)
    stacks_B[dest] += stacks_B[orig][-count:]
    stacks_B[orig] = stacks_B[orig][:len(stacks_B[orig]) - count]

# submit top stacks
top_stacks = ""
for stack in stacks_B.values():
    top_stacks += stack[-1]
submit(top_stacks, part='b', day=DAY, year=YEAR)
