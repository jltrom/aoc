from aocd.models import Puzzle
from aocd import submit

YEAR = 2022
DAY = 7


def build(inputs):
    """Return a node populated with entries from parsed inputs
    """
    node = {}
    skip = 0
    for i in range(len(inputs)):
        if skip:
            skip -= 1
            continue
        line = inputs[i]
        if line == "$ cd ..":
            return node, i + 1
        tokens = line.split(" ")
        if tokens[0] == "$":
            if tokens[1] == "ls":
                pass
            elif tokens[1] == "cd":
                node[tokens[2]], skip = build(inputs[i+1:])
        elif tokens[0] == "dir":
            pass
        else:
            # it's a file
            node[tokens[1]] = tokens[0]
    return node, len(inputs)

def sumfilter100k(node):
    """Return the sum of size of subdirectories < 100k
    """
    sum = 0
    sumfiltered = 0
    for k, v in node.items():
        if type(v) is dict:
            size, filteredsize = sumfilter100k(v)
            sumfiltered += filteredsize
        else:
            size = int(v)
        sum += size
    if sum <= 100_000:
        sumfiltered += sum
    return sum, sumfiltered


def findsmallest(node, limit):
    """Return the size of the smallest dir above the limit
    """
    sum = 0
    smallest = None
    for k, v in node.items():
        if type(v) is dict:
            size, tmp = findsmallest(v, limit)
            sum += size
            if tmp is not None:
                if smallest is None or tmp < smallest:
                    smallest = tmp
        else:
            sum += int(v)
    if smallest is None and sum >= limit:
        smallest = sum
    return sum, smallest


if __name__ == "__main__":
    puzzle = Puzzle(year=YEAR, day=DAY)
    cmds = puzzle.input_data.split("\n")
    tree, _ = build(cmds[1:])

    total, s100k = sumfilter100k(tree)
    submit(s100k, part='a', day=DAY, year=YEAR)

    limit = 30_000_000 - (70_000_000 - total)
    _, smallest = findsmallest(tree, limit)
    submit(smallest, part='b', day=DAY, year=YEAR)
