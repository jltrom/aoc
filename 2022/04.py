from aocd.models import Puzzle
from aocd import submit

YEAR = 2022
DAY = 4


def parse(line):
    """Parse and re-order the input line.

    Examples:
    >>> parse("21-81,20-21")
    [[20, 21], [21, 81]]
    >>> parse("4-6,4-4")
    [[4, 4], [4, 6]]
    >>> parse("4-6,6-6")
    [[4, 6], [6, 6]]
    """
    pair = [[int(tuple[0]), int(tuple[1])] for tuple in [assignment.split("-") for assignment in line.split(",")]]
    if pair[0][0] <= pair[1][0]:
        if pair[0][0] == pair[1][0] and pair[0][1] > pair[1][1]:
            return [pair[1], pair[0]]
        else:
            return pair
    else:
        return [pair[1], pair[0]]


def fully_contained(pair):
    """Return True if one range fully contain the other

    Examples:
    >>> fully_contained([[4, 5], [5, 7]])
    False
    >>> fully_contained([[4, 6], [6, 6]])
    True
    >>> fully_contained([[4, 4], [4, 6]])
    True
    """
    return pair[0][1] >= pair[1][1] or pair[1][0] == pair[0][0]


def overlap(pair):
    """Return True if the ranges overlap

    >>> overlap([[4, 4], [5, 6]])
    False
    >>> overlap([[4, 5], [5, 6]])
    True
    >>> overlap([[4, 4], [4, 6]])
    True
    """
    return pair[0][1] >= pair[1][0]


if __name__ == "__main__":
    puzzle = Puzzle(year=YEAR, day=DAY)
    assignments = [parse(line) for line in puzzle.input_data.split("\n")]

    nb_fully_contained = sum(
        fully_contained(assignment) for assignment in assignments
    )
    submit(nb_fully_contained, part='a', day=DAY, year=YEAR)

    nb_overlaped = sum(
        overlap(assignment) for assignment in assignments
    )
    submit(nb_overlaped, part='b', day=DAY, year=YEAR)
