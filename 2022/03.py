from aocd.models import Puzzle
from aocd import submit

YEAR = 2022
DAY = 3


def priority(item):
    """Return the priority of _item_.

    Examples:
    >>> [priority(i) for i in 'azAZ']
    [1, 26, 27, 52]
    """
    if 'a' <= item <= 'z':
        return ord(item) - 96
    else:
        return ord(item) - 38


def priority_rucksack(rucksack):
    """Return the priority of a _rucksack_.

    Examples:
    >>> priority_rucksack('vJrwpWtwJgWrhcsFMMfFFhFp')
    16
    >>> priority_rucksack('aaaBBB')
    Traceback (most recent call last):
        ...
    Exception: Common item not found
    """
    size = len(rucksack)
    compartment1 = rucksack[:size//2]
    compartment2 = rucksack[size//2:]
    for car in compartment1:
        if car in compartment2:
            return priority(car)
    raise Exception("Common item not found")


def priority_group(rs1, rs2, rs3):
    """Return the priority of the sticker (common item) from 3 rucksacks.

    Examples:
    >>> priority_group('vJrwpWtwJgWrhcsFMMfFFhFp', 'jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL', 'PmmdzqPrVvPwwTWBwg')
    18
    >>> priority_group('aaa' , 'abc', 'ddd')
    Traceback (most recent call last):
        ...
    Exception: Sticker not found
    """
    for car in rs1:
        if car in rs2:
            if car in rs3:
                return priority(car)
    raise Exception("Sticker not found")


if __name__ == "__main__":
    puzzle = Puzzle(year=YEAR, day=DAY)
    data = puzzle.input_data.split("\n")
    sumPriorities = sum(
        priority_rucksack(rucksack) for rucksack in data
    )
    submit(sumPriorities, part='a', day=DAY, year=YEAR)

    sumPriorities = sum(
        priority_group(data[start], data[start+1], data[start+2])
        for start in range(0, len(data), 3)
    )
    submit(sumPriorities, part='b', day=DAY, year=YEAR)
