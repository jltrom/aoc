from aocd import get_data, submit

YEAR = 2022
DAY = 9

shifting = {'U': [0, -1], 'D': [0, 1], 'R': [1, 0], 'L': [-1, 0]}


def mark(visited, knot):
    visited[knot[0], knot[1]] = 1


def move(xh, yh, xt, yt):
    dx = xh - xt
    dy = yh - yt
    if abs(dx) > 1 or abs(dy) > 1:
        if dx:
            xt += dx//abs(dx)
        if dy:
            yt += dy//abs(dy)
    return xt, yt


def pull(rope, visited1, visited9):
    for i in range(0, 9):
        rope[i+1][0], rope[i+1][1] = move(rope[i][0], rope[i][1], rope[i+1][0], rope[i+1][1])
        mark(visited1, rope[1])
        mark(visited9, rope[9])


data = get_data(day=DAY, year=YEAR).split("\n")

rope = [[0, 4] for _ in range(10)]
visited1, visited9 = {}, {}
mark(visited1, rope[1])
mark(visited9, rope[9])

for line in data:
    order, step = line.split(" ")
    for i in range(int(step)):
        rope[0][0] += shifting[order][0]
        rope[0][1] += shifting[order][1]
        pull(rope, visited1, visited9)

submit(len(visited1), part='a', day=DAY, year=YEAR)
submit(len(visited9), part='b', day=DAY, year=YEAR)
