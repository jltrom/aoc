from aocd import get_data, submit

YEAR = 2022
DAY = 8

map = get_data(day=DAY, year=YEAR).split("\n")
visi = [[False]*len(map[0]) for _ in range(len(map))]
for i in range(len(map)):
    row = map[i]
    left, right = 0, len(row) - 1
    maxLeft, maxRight = row[left], row[right]
    visi[i][left], visi[i][right] = True, True
    while left+1 < right:
        if maxLeft <= maxRight:
            left += 1
            if row[left] > maxLeft:
                maxLeft = row[left]
                visi[i][left] = True
        else:
            right -= 1
            if row[right] > maxRight:
                maxRight = row[right]
                visi[i][right] = True

for j in range(len(map[0])):
    col = [row[j] for row in map]
    top, bottom = 0, len(col) - 1
    maxTop, maxBottom = col[top], col[bottom]
    visi[top][j], visi[bottom][j] = True, True     
    while top+1 <= bottom:
        if maxTop <= maxBottom:
            top += 1
            if col[top] > maxTop:
                maxTop = col[top]
                visi[top][j] = True
        else:
            bottom -= 1
            if col[bottom] > maxBottom:
                maxBottom = col[bottom]
                visi[bottom][j] = True

nb_visible_trees = sum([sum(row) for row in visi])
submit(nb_visible_trees, part='a', day=DAY, year=YEAR)


def score(map, x, y):
    score = [0] * 4
    i = x-1
    height = map[x][y]
    while i >= 0:
        score[0] += 1
        if map[i][y] >= height:
            break
        i -= 1
    i = x + 1
    while i < len(map):
        score[1] += 1
        if map[i][y] >= height:
            break
        i += 1
    i = y-1
    while i >= 0:
        score[2] += 1
        if map[x][i] >= height:
            break
        i -= 1
    i = y + 1
    while i < len(map[0]):
        score[3] += 1
        if map[x][i] >= height:
            break
        i += 1
    return score[0] * score[1] * score[2] * score[3]


maxScore = 0
for i in range(len(map)):
    for j in range(len(map[0])):
        maxScore = max(maxScore, score(map, i, j))
submit(maxScore, part='b', day=DAY, year=YEAR)
