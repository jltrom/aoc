"""
Usage: scafolder <year> 

Scafold the project for a new advent of code year

Options:
  -h --help                   Show this screen.
"""
import os
from docopt import docopt


options = docopt(__doc__)
year = options["<year>"]

if not os.path.exists(year):
    os.makedirs(year)

for i in range(25):
    path = os.path.join(year, f"{i+1:02d}.py")
    if not os.path.exists(path):
        with open(path, "w") as file:
            file.write(f"""from aocd import get_data, submit

YEAR = {year}
DAY = {i+1}

if __name__ == "__main__":
    data = get_data(day=DAY, year=YEAR).split("\\n")
    submit(__answer_A__, part='a', day=DAY, year=YEAR)
    submit(__answer_B__, part='b', day=DAY, year=YEAR)
""")
