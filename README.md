# Advent of code ⭐⭐

This repository contains my submissions for [adventofcode](https://adventofcode.com)

* 2022 : [devlog](./2022/00.md) and [code](./2022)

## Usage 

#### To start a new year
```
> python scafolder.py 2022
```

#### To submit the answers for a given day 
```
❯ python -m 2022.01
Part a already solved with same answer: 74198
Part b already solved with same answer: 209914
```

#### To run doctest
```
> python -m doctest 2022/03.py -v
```